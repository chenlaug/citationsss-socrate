> **Citation 1 :**
> *Tout ce que je sais, c'est que je ne sais rien, tandis que les autres croient savoir ce qu'ils ne savent pas. *
> **Citation 2 :** *Existe-t-il pour l'homme un bien plus précieux que la Santé ?*
> **Citation 4 :** *Connais-toi toi-même.*
> **Citation 6 :** *La plus intelligente est celle qui sait qu'elle ne sait pas.*
> **Citation 8 :** *Une vie sans examen ne vaut pas la peine d'être vécue.*
> **Citation 10 :** *Existe-t-il pour l'homme un bien plus précieux que la Santé?*
> **Citation 12 :** *Nos jeunes aiment le luxe, ont de mauvaises manières, se moquent de l'autorité et n'ont aucun respect pour l'âge. A notre époque, les enfants sont des tyrans.*
